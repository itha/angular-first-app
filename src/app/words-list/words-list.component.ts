import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Word } from 'src/app/word';

@Component({
  selector: 'app-words-list',
  templateUrl: './words-list.component.html',
  styleUrls: ['./words-list.component.css']
})
export class WordsListComponent implements OnInit {

  @Input() loadedWordsList: Word[];
  @Output() updatedWordsListEvent = new EventEmitter<Word[]>();
  @Output() removeWordEvent = new EventEmitter<string>();

  newEnglishWord: string = '';
  newTranslations: string[] = [];
  // exampleList: any[] = [
  //     {"englishWord": 'toto', "translations": ['hoho', 'huhu']},
  //     {"englishWord": 'tata', "translations": ['haha', 'hehe']},
  // ]

  updateWord(wordToUpdate: Word, index: number): void {
    let newList: Word[] = this.loadedWordsList;
    if (wordToUpdate.englishWord !== '') {
      newList[index].englishWord = wordToUpdate.englishWord;
    }
    if (wordToUpdate.translations.length > 0) {
      newList[index].translations = wordToUpdate.translations;
    }
    this.updatedWordsListEvent.emit(newList);
    // this.removeWordEvent.emit(wordToUpdate);
  }

  removeWord(wordToRemove: Word): void {
    this.removeWordEvent.emit(wordToRemove.englishWord);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
