export class Word {

    constructor(
      public englishWord: string,
      public translations: string[]
    ) {  }
  
  }