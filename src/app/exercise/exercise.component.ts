import { Component, OnInit, Input } from '@angular/core';

import { Word } from "src/app/word";

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.css']
})
export class ExerciseComponent implements OnInit {
  @Input() loadedWordsList: object[];

  currentWord = new Word('', []);

  randomValue: number = 0;

  totalAnswers: number = 0;
  correctAnswers: number = 0;

  randomizeNewWord() {
    let newRandomValue: number = 0;
    let counter: number = 0;
    while (newRandomValue === this.randomValue && counter < 10) {
      // console.log(counter)
      newRandomValue = Math.floor(Math.random() * this.loadedWordsList.length);
      counter += 1;
    }
    this.randomValue = newRandomValue;

    if (this.loadedWordsList.length > 0) {
      this.currentWord.englishWord = this.loadedWordsList[this.randomValue]['englishWord'];
      this.currentWord.translations = [];
    }

  }

  checkWord(attemptWord: Word) {
    this.totalAnswers += 1;
    if (this.loadedWordsList[this.randomValue]['translations'].includes(attemptWord.translations[0])) {
      console.log('It\'s True !');
      this.correctAnswers += 1;
      this.randomizeNewWord();
      return 0;
    }
    console.log('It\'s false, sorry...')
    this.randomizeNewWord();
    return 1;
  }

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterContentInit(): void {
    this.randomizeNewWord();
  }

}
