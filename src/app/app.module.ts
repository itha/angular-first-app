import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WordComponent } from './word/word.component';
import { WordsListComponent } from './words-list/words-list.component';
import { ExerciseComponent } from './exercise/exercise.component';
import { WordFormComponent } from './word-form/word-form.component';

@NgModule({
  declarations: [
    AppComponent,
    WordComponent,
    WordsListComponent,
    ExerciseComponent,
    WordFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
