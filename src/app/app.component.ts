import { Component } from '@angular/core';

import { Word } from 'src/app/word';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'angular-first-app';
  wordsList: Word[] = [];
  newWord: Word;

  saveToFile() {
    if (this.wordsList.length < 1) {
      console.log('cannot generate file : list is empty !');
      return 1;
    }
    let homemadeJsonString = '{\n';
    this.wordsList.forEach(el => {
      homemadeJsonString += '    "' + el['englishWord'] + '": ["';
      homemadeJsonString += el['translations'].join('", "') + '"],\n';
    });
    homemadeJsonString = homemadeJsonString.slice(0, -2);
    homemadeJsonString += '\n}';
    homemadeJsonString = homemadeJsonString.replaceAll('\"\"', '');
    console.log(homemadeJsonString);
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(homemadeJsonString));
    element.setAttribute('download', 'words.json');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);

  }

  saveToLocalStorage(): void {
    localStorage.setItem('wordsList', JSON.stringify(this.wordsList));
    // console.log('saved to local storage : ', JSON.stringify(this.wordsList));
  }

  emptyList(): void {
    this.wordsList = []
    localStorage.removeItem('wordsList');
  }

  uploadAndParseJsonWordsFile(files: FileList) {
    let file: File = files.item(0);

    let reader: any = new FileReader();
    reader.onload = (e: any) => {
      let jsonObject: object = JSON.parse(e.target.result);

      Object.keys(jsonObject).forEach(englishWord => {
        // console.log(englishWord, jsonObject[englishWord]);
        this.wordsList.push(new Word(englishWord, jsonObject[englishWord]))
      });
      console.log(this.wordsList);
      // this.saveToLocalStorage();
    }
    reader.readAsText(file);
  }

  simulateUploadFileClick() {
    let el = document.getElementById('file-btn');
    if (el) {
      el.click();
    }
  }

  getNewWord(newWord: Word) {
    if (newWord.englishWord !== '' && newWord.translations.length !== 0) {
      this.wordsList.push(newWord);
      // this.saveToLocalStorage();
    }
  }

  getUpdatedList(newWordList: Word[]) {
    this.wordsList = newWordList;
    // this.saveToLocalStorage();
  }

  removeWordFromList(wordToRemove: string) {
    this.wordsList = this.wordsList.filter((value, index, arr) => {
      return value['englishWord'] !== wordToRemove;
    });
    // this.saveToLocalStorage();
  }

  ngOnInit() { // on init, get LocalStorage 'wordsList' and load it in the component
    let wordsListStr: string = localStorage.getItem('wordsList');
    let tempWordsList: object[] = JSON.parse(wordsListStr);
    if (tempWordsList !== null && tempWordsList.length > 0) {
      Object.keys(tempWordsList).forEach(index => {
        this.wordsList.push(new Word(tempWordsList[index]['englishWord'], tempWordsList[index]['translations']))
      });
    }
  }

  ngAfterContentChecked(): void {
    this.saveToLocalStorage();
  }
}
