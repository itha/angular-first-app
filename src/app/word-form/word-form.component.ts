import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { Word } from 'src/app/word';

@Component({
  selector: 'app-word-form',
  templateUrl: './word-form.component.html',
  styleUrls: ['./word-form.component.css']
})
export class WordFormComponent implements OnInit {

  @Output()
  postMessageEvent = new EventEmitter();

  // TODO: Remove this when we're done
  // get diagnostic() { return JSON.stringify(this.model); }

  // setTranslations(value: any) {
  //   this.model.translations = value.split(",");
  // }

  addNewWord(newWord: Word) {
    this.postMessageEvent.emit(newWord);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
