import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Word } from 'src/app/word';

@Component({
  selector: 'app-word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.css']
})
export class WordComponent implements OnInit {

  @Input() englishWordInput: string = '';
  @Input() translationsInput: string[] = [];

  @Input() englishWordLabel: string = 'English Word';
  @Input() translationsLabel: string = 'Translations';
  @Input() validateButtonLabel: string = 'Send';
  @Input() resetButtonLabel: string = 'Reset';
  @Input() footerLabel: string = 'Footer Text';

  @Output() validateActionEvent = new EventEmitter<Word>();
  @Output() denyActionEvent = new EventEmitter<Word>();

  wordModel: Word = new Word('', []);;

  setTranslations(value: any) {
    this.wordModel.translations = value.split(",");
  }

  sendValidateAction() {
    this.validateActionEvent.emit(this.wordModel);
    this.wordModel = new Word('', []);
  }

  sendDenyAction() {
    this.denyActionEvent.emit(this.wordModel);
    this.wordModel = new Word('', []);
  }

  returnSubmit(event: any) {
    if (event.keyCode === 13) {
      let button: any = document.getElementsByClassName('validate-button')[0];
      let input: any = document.getElementById('translations-input');
      button.click();
      input.blur();
    }
  }

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.wordModel = new Word(this.englishWordInput, this.translationsInput);
  }

}
